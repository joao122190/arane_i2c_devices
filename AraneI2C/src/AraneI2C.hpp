/*
    VERSÃO 1.0.1 DA BIBLIOTECA DE COMUNICAÇÃO I2C ARANE

    Desenvolvido por: Elevential IT Consulting
    Escrita por: Matheus Lima (matheus@elevential.com)

    Sujeita a Revisões Futuras. Favor, sempre explicitar a versão da biblioteca usada,
    Já que deverão existir atualizações futuras.
*/

#ifndef __ARANEI2C__
#define __ARANEI2C__
/*----------------------INCLUDE DE BIBLIOTECAS----------------------*/
#include <Wire.h>

/*---------------------MACRO DE DEBUG VIA SERIA---------------------*/
//#define __SERIALDEBUG__     /*Macro utilizada para debug serial*/

/*----------------------CONSTANTES DO PROTOCOLO----------------------*/
/*Definição de constantes: Em geral, eu prefiro separar as constantes em um arquivo separa,
mas como a ideia é que a biblioteca seja simples (e em poucos arquivos) estou fazendo aqui mesmo*/

/*Constante que define o tamanho de uma mensagem no nosso protocolo, em bytes*/
#define I2C_COMMUNICATION_SIZE      8

/*----------------------TIPOS DE DADOS----------------------*/

typedef byte size_i;

/*----------------------CLASSES----------------------*/

class AraneI2C{
    public:
        /*Construtor: Passa-se o tipo de nó(master=1/slave=0) e o endereço (necessário apenas para slave)*/
        /*Entrada:  bool mater(1)/slave(0)
                    size_i endereço (0x??)*/
        AraneI2C(bool master,size_i adress);

        /*Destrutor, só é necessário em caso de debug, para terminar a comunicação serial*/
        ~AraneI2C();

        /*transmit: transmite uma mensagem(array de char) para o endereço do slave (apenas para master)*/
        /*Entrada:  char* menssagem a ser transmitida pelo protocolo (tamanho definido por I2C_COMMUNICATION_SIZE)
                    size_i& endereço do slave (0x??)
          Saída:    AraneI2C& (isso permite concatenar chamadas, ex: Obj.transmit(..).transmit(..).transmit(..)...)*/
        AraneI2C& transmit(char* message,size_i& slave_adress);
        
        /*request: retorna um ponteiro para char array do escravo*/
        /*ATENÇÃO: NÃO ESQUECER DE DAR FREE NO PONTEIRO*/
        /*Entrada:  size_i& endereço do Slave (0x??)
          Saída:    char* menssagem (tamanho definido por I2C_COMMUNICATION_SIZE)*/
        char* request(size_i& slave_adress);

        /*debugBegin: Apenas um método que inicializa o monitor serial*/
        /*Entrada:  void
          Saída:    AraneI2C& (permite concatenar chamadas)*/
        AraneI2C& debugBegin();

        /*Função a ser chamada quando master requer*/
        /*Entrada:  void(*function)(void) (Uma função com entrada E saída void)
          Saída     AraneI2C& (permite concatenar chamadas)*/
        AraneI2C& onRequest(void(*function)(void));

        /*Função a ser chamada quando o slave receber a menssagem*/
        /*Entrada:  void(*function)(void) (Uma função com entrada E saída void)
          Saída     AraneI2C& (permite concatenar chamadas)*/
        AraneI2C& onReceive(void(*function)(void));

        /*Função que retorna se a mensagem ainda está disponível*/
        /*Entrada:  void
          Saída     int*/
        int available();

        /*Escreve no barramento para o master*/
        /*Entrada:  size_i
          Saída     AraneI2C& (permite concatenar chamadas)*/
        AraneI2C& write(size_i);

        /*Lê dados do barramento*/
        /*Entrada:  void
          Saída     int*/
        int read();

    private:
        const bool i2c_master;
        const size_i i2c_adrr;
};
#endif

/*----------------------MACROS DE DEBUG----------------------*/
/*só é necessário declara-las caso quisermos debugar o código*/
#ifdef __SERIALDEBUG__
#define DEBUG_SERIALFREQUENCY       9600
#endif