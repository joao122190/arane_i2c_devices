#include <Arduino.h>
#include <stdio.h>
#include "MemoryFree.h"

#include "AraneI2C.hpp"

AraneI2C Master(true,0x00);
char* menssagem;
size_i address = 0x01;

void setup(){
    Master.debugBegin();  /*Descomentar isso && a macro __SERIALDEBUG__ para debugar o código pela porta serial*/
}

void loop(){
    menssagem = Master.request(address);
    free(menssagem);       /*Não esquecer de dar free no ponteiro!!!!*/

    Serial.begin(9600);
    Serial.print("Memoria RAM Livre: ");
    Serial.println(freeMemory());
    delay(1000);
}