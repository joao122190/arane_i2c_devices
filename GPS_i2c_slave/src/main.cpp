
#include <Wire.h>
#include <Arduino.h>
#include <avr/wdt.h>
//#include <TimeLib.h>
#include "TinyGPS.h"
#include <SoftwareSerial.h>

//ARANE ADRESS LIST(03-7f or 03-127)124 options:

#define GPS_DEVICE_ADDR 3
#define XBEE_DEVICE_ADDR 4
#define VEHICLE_PAINEL_DEVICE_ADDR 5

//Arduino Digital reader: avaliar o uso de multiplex para reduzir o numero de portas i2c
#define DIGITAL_PORTS_READER_1 40
#define DIGITAL_PORTS_READER_2 41
#define DIGITAL_PORTS_READER_3 42
#define DIGITAL_PORTS_READER_4 43

//ADC devices
#define ADS1115_1 72
#define ADS1115_2 73
#define ADS1115_3 74
#define ADS1115_4 75 


/**GPS definition**/
#define COOR_CONV_FACTOR 1000000
#define SPEED_CONV_FACTOR 10
#define STACK_SIZE 60

#define GPS_RX A1
#define GPS_TX A0
TinyGPS gps;
SoftwareSerial gps_serial(GPS_RX, GPS_TX);//rxPin,txPin

//Global vars
String data_to_send = "";
byte index_of_data = 0;

//this stack will be reseted each 1 hour
/* gps_data_stack
  variable           size      addr
  d_lat              int         0
  d_lon              int         1
  alt                int         2
  speed              int         3
  course             int         4
  total              10 bytes

  gps_timestamp_Stack
  nano_epoch                long 
  total                   4 bytes
*/
//

bool stack_is_full = 0;
byte last_second_control = 0;
byte stack_index_control = 0;
byte older_data_index_stack = 0;
byte next_data_index_stack_to_save = 0;
int gps_data_stack[STACK_SIZE][7];
float gps_timestamp_stack[STACK_SIZE];

///declare reference_gps_values
float ref_lat = 0.0;
float ref_lon = 0.0;
float ref_speed = 0;
byte today = 0;
byte ref_month=0;
byte ref_year =0;


/////////////////////////////////////////////
/*********** General functions *************/
/////////////////////////////////////////////
void process_master_request(String req ){
  return;
}
void reset_system(){
	delay(10000);
}
/////////////////////////////////////////////
/*************** GPS functions *************/
/////////////////////////////////////////////
void gps_delay(unsigned int ms = 1000) {
	unsigned long start = millis();
	do {
		wdt_reset();
		while (gps_serial.available())gps.encode(gps_serial.read());
	} while (millis() - start < ms);
}

bool get_gps_delta_ref_position(int *d_lat, int *d_lon, int *alt, int *d_speed , int *course, 
							    byte *da, byte *ho, byte *mi, byte *se ) {

	gps_delay(100);
	float lat, lon, speed ;
	int year;
	byte month, day, hour, minute, second;
	
	gps.crack_datetime(&year, &month, &day, &hour, &minute, &second);
	gps.f_get_position(&lat, &lon);// retrieves +/- lat/long in 100000ths of a degree
	speed = gps.f_speed_kmph();
  	
	//Serial.println(lat,4);
	if (ref_lat==0 or ref_lat==1000 or ref_speed==-1){
		ref_lat = lat;
		ref_lon = lon;
		ref_speed = speed;
		today = day;
		ref_month= month;
		ref_year = byte(year-2000);
		return 0;

	}else{
		*d_lat = ( ref_lat - lat )*COOR_CONV_FACTOR;
		*d_lon = ( ref_lon - lon )*COOR_CONV_FACTOR;
		*alt =  int(gps.f_altitude());
		*d_speed = int(( ref_speed - speed )*SPEED_CONV_FACTOR);
		*course = int(gps.f_course());
		*da = day;
		*ho = hour;
		*mi = minute;
		*se = second;
		return 1;
	}

}

/////////////////////////////////////////////
/*********** i2c functions *************/
/////////////////////////////////////////////

//this functions rise when the i2c master request, and send one byte
void requestEvent() {
  if (data_to_send==""){return;}
   
  if(index_of_data == data_to_send.length()){
    data_to_send = "";
    index_of_data = 0;
    return;
  }
  char c = data_to_send[index_of_data];
  Wire.write(c);
  index_of_data++;


}

//this function rises when the slave need send something
void receiveEvent(int sizeOfdata) {
  String req_data = "";
  while (Wire.available()) {
    req_data = req_data+Wire.read();
  } 
  process_master_request(req_data);
}
  
void assembly_data_to_send(byte index){
	//this function will assembly the data_in_protocol, it will get a raw from stack and put in a global var
	int d_lat = gps_data_stack[index][0];
	int d_lon = gps_data_stack[index][1];
	int alt = gps_data_stack[index][2];
	int d_speed = gps_data_stack[index][3];
	int course = gps_data_stack[index][4];

	long timestamp = gps_timestamp_stack[index];
	int hour = int(timestamp/3600);
	int minute = int((timestamp%3600)/60);
	int second = timestamp - long(hour*3600) - minute*60;

	data_to_send = String(ref_lat+(-1*float(d_lat)/COOR_CONV_FACTOR))+"|"+
	String(ref_lon+(-1*float(d_lon)/COOR_CONV_FACTOR))+"|"+
	String(alt)+"|"+
	String(ref_speed+(-1*float(d_speed)/SPEED_CONV_FACTOR))+"|"+
	String(course)+"|"+
	String(2000+ref_year)+"-"+String(ref_month)+"-"+String(today)+"T"+
	String(hour)+":"+String(minute)+":"+String(second)+"Z";
	
	
	/*Serial.print("ref_speed: ");
	Serial.println(ref_speed,6);
	Serial.println("d_speed : "+String(d_speed));
	Serial.print("recovery_speed: ");
	float rec_diff = float(d_speed)/SPEED_CONV_FACTOR;
	Serial.println(ref_speed+(-1*rec_diff),6);
	*/
  	
	  
 
}

void setup() {
  wdt_enable(WDTO_8S);  //watchdog to 8sec
  gps_serial.begin(9600);
  Wire.begin(GPS_DEVICE_ADDR); // enable this device as slave in port 8 of the bus         
  Wire.onRequest(requestEvent); // register event
  Wire.onReceive(receiveEvent); // register event
  Serial.begin(9600);
}
void loop() {
	
	gps_delay(300);

	int i;
	for(i=0;i<STACK_SIZE;i++){
		if(gps_timestamp_stack[i]<gps_timestamp_stack[older_data_index_stack] &&
		gps_timestamp_stack[i]!=0){	
			older_data_index_stack=i;
		}
	}
	
	next_data_index_stack_to_save = -1;
	for(i=0;i<STACK_SIZE;i++){
		if(gps_timestamp_stack[i]==0){	
			next_data_index_stack_to_save=i;
			break;
		}
	}
	if (next_data_index_stack_to_save == -1 or next_data_index_stack_to_save==255){
		next_data_index_stack_to_save = older_data_index_stack;
	}

	//prepare data to i2c comunication
	assembly_data_to_send(older_data_index_stack);
	Serial.println(data_to_send);

	int d_lat, d_lon, alt, course, d_speed;
	byte day,hour,minute,second;
  	if(get_gps_delta_ref_position(&d_lat,&d_lon,&alt,&d_speed,&course,&day,&hour,&minute,&second)){
		if(today != day){reset_system();}
		
		if(last_second_control != second){
			last_second_control = second;
		
			long timestamp = (long(hour)*60*60)+(int(minute)*60)+int(second);

			Serial.println("older "+String(older_data_index_stack)+" next: "+String(next_data_index_stack_to_save)  );

		    Serial.print(String(d_lat)+"-"+String(d_lon)+"-"+String(alt)+"-"+String(d_speed)+"-"+String(course));
			Serial.print("  "+String(day)+"<->"+String(hour)+":"+String(minute)+":"+String(second));
			Serial.println(" "+String(timestamp) );

			// fill gps_data_stack 
			gps_data_stack[next_data_index_stack_to_save][0] = int(d_lat);
			gps_data_stack[next_data_index_stack_to_save][1] = int(d_lon);
			gps_data_stack[next_data_index_stack_to_save][2] = int(alt);
			gps_data_stack[next_data_index_stack_to_save][3] = int(d_speed);
			gps_data_stack[next_data_index_stack_to_save][4] = int(course);
		
			// fill gps_timestamp_stack 
			gps_timestamp_stack[next_data_index_stack_to_save]=timestamp;	

		}

	}

}

