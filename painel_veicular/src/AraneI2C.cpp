#include <Arduino.h>
#include <Wire.h>
#include "AraneI2C.hpp"

AraneI2C::AraneI2C(bool master = false, size_i adress = 0) : i2c_master(master),i2c_adrr(adress){
    /*Inicializa comunicação I2C*/
    if (i2c_master == true)
        Wire.begin();
        else
        Wire.begin(i2c_adrr);

    #ifdef  __SERIALDEBUG__
    Serial.begin(DEBUG_SERIALFREQUENCY);
    Serial.println();
    Serial.print("Initializing I2C Protocol Comunication as ");
    if (i2c_master == false)
        Serial.print("slave");
        else
        Serial.print("master");
    Serial.print(" at address ");
    Serial.print(i2c_adrr);
    Serial.print("\n");
    Serial.flush();
    #endif
};

AraneI2C::~AraneI2C(){
    #ifdef __SERIALDEBUG__
    Serial.println("Terminating Serial comunication");
    Serial.flush();
    Serial.end();
    #endif
}

AraneI2C& AraneI2C::transmit(char* message,size_i& slave_adress){
    if (i2c_master == 1){
        Wire.beginTransmission(slave_adress);
        Wire.write(message);
        Wire.endTransmission();

        #ifdef  __SERIALDEBUG__
        Serial.print("Transmited Message: ");
        Serial.print(message);
        Serial.print(" to Address ");
        Serial.println(slave_adress);
        Serial.flush();
        #endif
        return *this;
    }
    #ifdef  __SERIALDEBUG__
    Serial.print("Error: Only the master node can transmit messages\n");
    #endif
    return *this;
};

char* AraneI2C::request(size_i& slave_adress){
    if (i2c_master == 1){
        Wire.requestFrom((uint8_t) slave_adress,(uint8_t) I2C_COMMUNICATION_SIZE);
                
        char* message = (char*) malloc(I2C_COMMUNICATION_SIZE+1);   //Adiciona o caracter NULL
        size_i i = 0;
        while(Wire.available() && i < I2C_COMMUNICATION_SIZE)   //Esse && é apenas para, mesmo em caso de erro, a instrução não tente acessar um espaço de memória não alocado
        {
            message[i] = Wire.read();
            i++;
        };
        message[i] = '\0';      /*Adiciona o character NULL ao final da menssagem*/
        #ifdef  __SERIALDEBUG__
        Serial.print(i);
        Serial.print(" Characters Received: ");
        Serial.println(message);
        Serial.flush();
        #endif
        return message;
    }
    #ifdef  __SERIALDEBUG__
    Serial.println("Error: Only the master can request messages");
    Serial.flush();
    #endif
    return NULL;
};

AraneI2C& AraneI2C::debugBegin(){
    #ifdef  __SERIALDEBUG__
    Serial.begin(DEBUG_SERIALFREQUENCY);
    #endif
    return *this;
}

AraneI2C& AraneI2C::onRequest(void(*function)(void)){
    if (i2c_master == 0)
    {
        Wire.onRequest(function);
        return *this;
    }
    #ifdef  __SERIALDEBUG__
    Serial.print("Error: Only slave nodes can respond on request\n");
    Serial.flush();
    #endif
    return *this;
}

AraneI2C& AraneI2C::onReceive(void(*function)(void)){
    if (i2c_master == 0)
    {
        Wire.onRequest(function);
        return *this;
    }
    #ifdef  __SERIALDEBUG__
    Serial.print("Error: Only slave nodes can respond on received\n");
    Serial.flush();
    #endif
    return *this;
}

int AraneI2C::available(){
    return Wire.available();
}

AraneI2C& AraneI2C::write(size_i response){
    Wire.write(response);
    return *this;
}

int AraneI2C::read(){
    return Wire.read();
}