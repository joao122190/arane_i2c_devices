#include <Arduino.h>
#include <stdlib.h>
#include "const.hpp"
#include "dataTypes.hpp"
#include "auxFnc.hpp"

/*-----------------Functions-----------------*/

errorType iniciatilizePins(){

    /*Initialize led pins*/
    pinMode(PIN_LED_1, OUTPUT);
    pinMode(PIN_LED_2, OUTPUT);
    pinMode(PIN_LED_3, OUTPUT);
    pinMode(PIN_LED_4, OUTPUT);
    pinMode(PIN_LED_R, OUTPUT);
    pinMode(PIN_LED_G, OUTPUT);
    pinMode(PIN_BUZZER,OUTPUT);

    /*Initialize rfid pin*/
    pinMode(PIN_RFID_INT,INPUT);
    pinMode(PIN_RFID_RX,INPUT);
    pinMode(PIN_RFID_TX,OUTPUT);


    /*Attach interrup to rfid interrupt pin*/
    attachInterrupt(digitalPinToInterrupt(PIN_RFID_INT),rfidInterrupt,RFID_INTERRUPTION_MODE);

    return OK;
}

void rfidInterrupt(){
}

errorType executeRoutine(char* routineName,const LED& LedObject){
    if (strcmp(routineName,LED_1_TURNON) == 0){
        LedObject.write(PIN_LED_1,true);
        return OK;
    }
    if (strcmp(routineName,LED_2_TURNON) == 0){
        LedObject.write(PIN_LED_2,true);
        return OK;
    }
    if (strcmp(routineName,LED_3_TURNON) == 0){
        LedObject.write(PIN_LED_3,true);
        return OK;
    }
    if (strcmp(routineName,LED_4_TURNON) == 0){
        LedObject.write(PIN_LED_4,true);
        return OK;
    }
    if (strcmp(routineName,LED_1_TURNOFF) == 0){
        LedObject.write(PIN_LED_1,false);
        return OK;
    }
    if (strcmp(routineName,LED_2_TURNOFF) == 0){
        LedObject.write(PIN_LED_2,false);
        return OK;
    }
    if (strcmp(routineName,LED_3_TURNOFF) == 0){
        LedObject.write(PIN_LED_3,false);
        return OK;
    }
    if (strcmp(routineName,LED_4_TURNOFF) == 0){
        LedObject.write(PIN_LED_4,false);
        return OK;
    }
    if (strcmp(routineName,LED_RG_TURNR) == 0){
        LedObject.writeColor(RED);
        return OK;
    }
    if (strcmp(routineName,LED_RG_TURNG) == 0){
        LedObject.writeColor(GREEN);
        return OK;
    }

return invalidRoutine;
}

/*------------------Classes------------------*/
errorType LED::write(int PIN_NAME,bool Value) const{
    switch(PIN_NAME){   /*check if pin is a valid LED pin. If not, returns an error code*/
        case PIN_LED_1:
        case PIN_LED_2:
        case PIN_LED_3:
        case PIN_LED_4:
        case PIN_LED_G:
        case PIN_LED_R:
            digitalWrite(PIN_NAME,Value);
            return OK;

        default:
            return notValidLed; 
        };
    return unknownError;
}

errorType LED::writeColor(ledColor color) const{
    if (color == RED){
        digitalWrite(PIN_LED_R,HIGH);
        digitalWrite(PIN_LED_G,LOW);
        return OK;
    }
    if (color == GREEN){
        digitalWrite(PIN_LED_R,LOW);
        digitalWrite(PIN_LED_G,HIGH);
        return OK;
    }
    return notValidColor;
}