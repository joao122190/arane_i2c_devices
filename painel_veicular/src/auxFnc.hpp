#include "const.hpp"
#include "dataTypes.hpp"

#ifndef __AUXFNC__
#define __AUXFNC__

/*------------------Classes------------------*/

/*Classe que contém os métodos relacionados a LEDs*/
class LED {

    public:
        /*Method that writes to an LED output*/
        /*Entradas:     MACRO de PIN_NAME
                        bool Valor
          Saídas:       Código de Erro*/
        errorType write(int PIN_NAME,bool Value) const;

        /*Method that writes to the RG Led output the desired color*/
        /*Entradas:     Cor do LED
          Saídas:       Código de Erro*/
        errorType writeColor(ledColor color) const;
};

/*-----------------Functions-----------------*/

/*Função que inicializa os pinos do ATMEGA como entrada, saída, etc*/
/*Entrada: void
  Saídas: Código de erro*/
errorType iniciatilizePins();

/*Função da rotina de interrupção do rfid;
essa função deve não tomar nenhum parâmetro nem retornar nada.
Essa função é chamada de rotina de serviço da interrupção, ou ISR*/
/*Entrada:  void
  Saída:    void*/
void rfidInterrupt();


/*Essa função executa rotinas de software ao ser chamada por uma interrupção de comunicação*/
/*Entrada:      char* Ordem recebida
                const LED& objeto Led
                
  Saída:        código de erro*/
errorType executeRoutine(char*,const LED& LedObject);

#endif