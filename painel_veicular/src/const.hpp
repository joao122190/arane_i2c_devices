#ifndef __CONSTS__
#define __CONSTS__

#define __SERIALDEBUG__                 /*MACRO para debug do código por uma saída serial*/

#define PIN_SDA                     27
#define PIN_SCL                     28
#define PIN_GPS_TX                  15  /*Ainda preciso trocar na PCB*/
#define PIN_BUZZER                  14  /*Ainda preciso trocar na PCB*/

#define PIN_LED_R                   16  /*Ainda preciso trocar na PCB*/
#define PIN_LED_G                   5
#define PIN_LED_1                   6
#define PIN_LED_2                   11
#define PIN_LED_3                   12
#define PIN_LED_4                   13

#define PIN_RFID_RX                 2   /*Ainda preciso trocar na PCB*/
#define PIN_RFID_TX                 3   /*Ainda preciso trocar na PCB*/
#define PIN_RFID_INT                4   /*Ainda preciso trocar na PCB*/
#define PIN_MOSI                    17
#define PIN_MISO                    18

#define RFID_INTERRUPTION_MODE      LOW /*Pode ser: LOW, CHANGE, RISING ou FALLING*/


/*----------------------------NOME DAS ROTINAS DE INTERRUPÇÃO----------------------------*/
/*CADA UMA DAS STRINGS ABAIXO CORRESPONDE A UM TIPO DE ROTINA DE INTERRUPÇÃO A SER RODADA*/

#define LED_1_TURNON                "led1on\0\0\0"
#define LED_2_TURNON                "led2on\0\0\0"
#define LED_3_TURNON                "led3on\0\0\0"
#define LED_4_TURNON                "led4on\0\0\0"
#define LED_1_TURNOFF               "led1off\0\0"
#define LED_2_TURNOFF               "led2off\0\0"
#define LED_3_TURNOFF               "led3off\0\0"
#define LED_4_TURNOFF               "led4off\0\0"
#define LED_RG_TURNR               "ledrgr\0\0\0"
#define LED_RG_TURNG               "ledrgg\0\0\0"

#endif