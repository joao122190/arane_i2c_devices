#ifndef __DATATYPES__
#define __DATATYPES__

typedef enum{ 
    OK,
    unknownError,
    notValidLed,
    notValidColor,
    invalidRoutine
} errorType;

typedef enum{
    RED,
    GREEN
} ledColor;

#endif