#include <Arduino.h>
#include "const.hpp"
#include "dataTypes.hpp"
#include "auxFnc.hpp"
#include "AraneI2C.hpp"
#include "dataTypes.hpp"

/*Protótipo de algumas funções*/
void onReceive(); /*Rotina a ser rodada quando o Master enviar dados*/
void onRequest(); /*Rotina a ser rodada quando o Master requerer dados*/

/*Crio um objeto de comunicação AraneI2C, do tipo slave e endereço 0x01*/
AraneI2C Painel(false,0x01);
/*Crio um objeto de controle de Leds 'LED'*/
LED Led();

void setup() {
  iniciatilizePins(); /*Inicializa o pinout do arduino*/
  
  Painel.onReceive(onReceive);  /*Funções que decidem o que é feito ao se receber/requerer dados do mestre*/
  Painel.onRequest(onRequest);
}

void loop() {
}


void onReceive()
{
  /*Essa função lerá aquilo que o mestre está enviando, para que o painel possa saber qual ação deverá ser tomada*/

  /*Ler dados que o Master está enviando*/
  /*Aloca memória para a leitura de qual rotina deve ser executada*/
  char* routine = (char*) malloc(I2C_COMMUNICATION_SIZE+1);   //Adiciona o caracter NULL
  size_i i = 0;
  while(Painel.available() && i < I2C_COMMUNICATION_SIZE)   //Esse && é apenas para, mesmo em caso de erro, a instrução não tente acessar um espaço de memória não alocado
  {
    routine[i] = Painel.read();
    i++;
  };
  routine[i] = '\0';      /*Adiciona o character NULL ao final da menssagem*/

  /*Executa a rotina desejada*/
  executeRoutine(routine,LED());

  /*Não esquecer de liberar o ponteiro de rotina*/
  free(routine);
}

void onRequest()
{
}